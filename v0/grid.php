<?php
/**
 * This is the gridsquare LibreHam API query call.
 * This implements the LibreHam APIv0 grid.php
 *
 * @package    LibreHam
 * @subpackage grid api
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2023 LibreHam Project
 * @since      0.0.2
 */

require_once '../vendor/autoload.php';
require_once 'GeoMath.inc';

header('Content-Type: application/json; charset=utf-8');
$outputArray = [];

if (isset($_GET['grid']) === true && empty($_GET['grid']) === false) {
    $latLong     = GeoMath::gridToLatLong($_GET['grid']);
    $outputArray = [
        'grid'  => $_GET['grid'],
        'coord' => $latLong,
    ];
}

if (isset($_GET['lat']) === true && isset($_GET['long']) === true
    && empty($_GET['lat']) === false && empty($_GET['long']) === false
) {
    $grid        = GeoMath::latLongToGrid($_GET['lat'], $_GET['long']);
    $outputArray = [
        'grid'  => $grid,
        'coord' => [
            'lat'  => $_GET['lat'],
            'long' => $_GET['long'],
        ],
    ];
}

if (isset($_GET['mygrid']) === true && isset($_GET['dxgrid']) === true
    && empty($_GET['mygrid']) === false && empty($_GET['dxgrid']) === false
) {
    $unit = 'KM';
    if (isset($_GET['unit']) === true && empty($_GET['unit']) === false) {
        $unit = $_GET['unit'];
    }

    $myLatLong   = GeoMath::gridToLatLong($_GET['mygrid']);
    $dxLatLong   = GeoMath::gridToLatLong($_GET['dxgrid']);
    $distance    = GeoMath::greatCircleDistance(
        $myLatLong['lat'],
        $myLatLong['long'],
        $dxLatLong['lat'],
        $dxLatLong['long'],
        $unit
    );
    $heading     = GeoMath::greatCircleBearing(
        $myLatLong['lat'],
        $myLatLong['long'],
        $dxLatLong['lat'],
        $dxLatLong['long']
    );
    $outputArray = [
        'myLoc'    => [
            'grid'  => $_GET['mygrid'],
            'coord' => $myLatLong,
        ],
        'dxLoc'    => [
            'grid'  => $_GET['dxgrid'],
            'coord' => $dxLatLong,
        ],
        'units'    => $unit,
        'distance' => $distance,
        'heading'  => $heading,

    ];
}//end if

echo json_encode($outputArray);

\Sentry\captureLastError();
