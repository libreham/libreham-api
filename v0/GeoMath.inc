<?php
 /**
  * This is a math library for calculations and conversions.
  *
  * Author     Matthew Chambers <mchambers@mchambersradio.com>
  * Copyright  2023 LibreHam Project
  * Since      0.0.1
  */
class GeoMath
{


    /**
     * Function converts Lat/Long pair to Maidenhead Square.
     * Maiddenhead = fLon.fLat.gLon.gLat.sLon.sLat;.
     * The following is based on HamGridSquare.js.
     * Original Copyright 2014 Paul Brewer KI6CQ.
     * Under a MIT License.
     * Re-written into PHP by Matthew Chambers NR0Q.
     *
     * @param float $lat  A valid Latitude coordinate.
     * @param float $long A valid Longitude coordinate.
     *
     * @return string
     */
    public static function latLongToGrid(float $lat, float $long) : string
    {
        $u = 'ABCDEFGHIJKLMNOPQRSTUVWX';
        if (is_nan($lat) === true) {
            \Sentry\captureMessage('Latitude is not a number: '.$lat);
            die();
        }

        if (is_nan($long) === true) {
            \Sentry\captureMessage('Longitude is not a number: '.$long);
            die();
        }

        if (abs($lat) === 90.0) {
            \Sentry\captureMessage('Unable to calculate gridsquare at the poles!');
            die();
        }

        if (abs($lat) > 90) {
            \Sentry\captureMessage('Invalid latitude '.$lat.' > 90 or < -90!');
            die();
        }

        if ($long > 180) {
            $tmp  = ($long + 360);
            $tmp  = ($tmp % 360);
            $long = ($tmp - 360);
        } else if ($long < -180) {
            while ($long < -180) {
                $long += 180;
                $long  = (180 + $long);
            }
        }

        $adjLat  = ($lat + 90);
        $adjLong = ($long + 180);
        $fLat    = $u.substr(floor($adjLat / 10), 1);
        $fLong   = $u.substr(floor($adjLong / 20), 1);
        $gLat    = floor($adjLat % 10);
        $gLong   = floor(($adjLong / 2) % 10);
        $sLat    = $u.substr((($adjLat - floor($adjLat) * 60) / 2.5), 1);
        $sLong   = $u.substr(((($adjLong - 2 * floor($adjLong / 2)) * 60) / 5), 1);
        return $fLat.$fLong.$gLat.$gLong.$sLat.$sLong;

    }//end latLongToGrid()


    /**
     * Function converts Maidenhead Square to Lat/Long pair.
     * Accurate to center of the gridsquare.
     * Maiddenhead = fLon.fLat.gLon.gLat.sLon.sLat;.
     *
     * @param string $grid Gridsquare in 4 or 6 char format.
     *
     * @return array An assoc array containing lat and long of center of gridsquare.
     */
    public static function gridToLatLong(string $grid) : array
    {
        $grid       = strtoupper($grid);
        $gridLength = strlen($grid);
        $a          = (ord(substr($grid, 0, 1)) - 65);
        $b          = (ord(substr($grid, 1, 1)) - 65);
        $c          = (ord(substr($grid, 2, 1)) - 48);
        $d          = (ord(substr($grid, 3, 1)) - 48);
        $lat1       = ($b * 10 + $d);
        $long1      = ($a * 20 + $c * 2);
        if ($gridLength === 4) {
            $lat2  = ($lat1 + 1);
            $long2 = ($long1 + 2);
        } else {
            $e      = (ord(substr($grid, 4, 1)) - 65);
            $f      = (ord(substr($grid, 5, 1)) - 65);
            $r      = (5 / 60);
            $t      = (2.5 / 60);
            $long3  = (($e * 5) / 60);
            $lat3   = (($f * 2.5) / 60);
            $lat1  += $lat3;
            $long1 += $long3;
            $lat2   = ($lat1 + $t);
            $long2  = ($long1 + $r);
        }

        $lat1  -= 90;
        $long1 -= 180;
        $lat2  -= 90;
        $long2 -= 180;
        $lat    = ($lat2 - ($lat2 - $lat1) / 2);
        $long   = ($long2 - ($long2 - $long1) / 2);
        return [
            'lat'  => $lat,
            'long' => $long,
        ];

    }//end gridToLatLong()


    /**
     * Private function to validate units and return radius of Earth in provided units.
     *
     * @param string $unit Units of Meters, Kilometers, Statue Miles, Nautical Miles, Yards, Feet or Degrees.
     *
     * @return float Returns the radius of the Earth in the Units provided
     */
    private static function greatCircleValidRadius(string $unit) : float
    {
        $r = [
            'M'  => 6371009,
            'KM' => 6371.009,
            'MI' => 3958.761,
            'NM' => 3440.070,
            'YD' => 6967420,
            'FT' => 20902260,
            'DG' => 57.2958,
        ];
        if (array_key_exists($unit, $r) === true) {
            return $r[$unit];
        } else {
            return 0;
        }

    }//end greatCircleValidRadius()


    /**
     * Function to find the distnace between 2 sets of Lat/Long coordinates.
     *
     * @param float  $lat1  The latitude of the 1st point.
     * @param float  $long1 The longitude of the 1st point.
     * @param float  $lat2  The latitude of the 2nd point.
     * @param float  $long2 The longitude of the 2nd point.
     * @param string $unit  Units of Meters, Kilometers, Statue Miles, Nautical Miles, Yards, Feet or Degrees.
     *
     * @return float Returns the distance between the 2 points provided.
     */
    public static function greatCircleDistance(
        float $lat1,
        float $long1,
        float $lat2,
        float $long2,
        string $unit='KM'
    ) : float {
        $r         = self::greatCircleValidRadius($unit);
        $lat1     *= (pi() / 180);
        $long1    *= (pi() / 180);
        $lat2     *= (pi() / 180);
        $long2    *= (pi() / 180);
        $longDelta = abs($long2 - $long1);
        $a1        = pow((cos($lat2) * sin($longDelta)), 2);
        $a2        = pow((cos($lat1) * sin($lat2) - sin($lat1) * cos($lat2) * cos($longDelta)), 2);
        $a         = ($a1 + $a2);
        $b         = (sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos($longDelta));
        $angle     = atan2(sqrt($a), $b);
        return ($angle * $r);

    }//end greatCircleDistance()


    /**
     * Function to find the bearing between 2 sets of Lat/Long coordinates.
     *
     * @param float $lat1  The latitude of the 1st point.
     * @param float $long1 The longitude of the 1st point.
     * @param float $lat2  The latitude of the 2nd point.
     * @param float $long2 The longitude of the 2nd point.
     *
     * @return float Returns the bearing between the 2 points provided.
     */
    public static function greatCircleBearing(
        float $lat1,
        float $long1,
        float $lat2,
        float $long2
    ) : float {
        $lat1     *= (pi() / 180);
        $long1    *= (pi() / 180);
        $lat2     *= (pi() / 180);
        $long2    *= (pi() / 180);
        $longDelta = ($long2 - $long1);
        $y         = (sin($longDelta) * cos($lat2));
        $x         = (cos($lat1) * sin($lat2) - sin($lat1) * cos($lat2) * cos($longDelta));
        $brng      = atan2($y, $x);
        $brng      = ($brng * (180 / pi()));
        if ($brng < 0) {
            $brng += 360;
        }

        return $brng;

    }//end greatCircleBearing()


}//end class
