<?php
/**
 * This is the geocode LibreHam API query call.
 * This implements the LibreHam APIv0 geocode.php
 *
 * @package    LibreHam
 * @subpackage geocode api
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2023 LibreHam Project
 * @since      0.0.2
 */

 require_once '../vendor/autoload.php';
require_once 'GeoMath.inc';

mysqli_report(MYSQLI_REPORT_ERROR);
header('Content-Type: application/json; charset=utf-8');

$server = '104.197.89.54';
$user   = 'uls';
$pass   = 'bC97l5MaAdoavF2s';
$hamdb  = 'hamdb';
$ulsdb  = 'ULSDATA';

$outputArray = [];

$connUls = new mysqli($server, $user, $pass, $ulsdb);
if (mysqli_connect_error() === true) {
    \Sentry\captureMessage(mysqli_connect_error());
    die();
}

if (isset($_GET['zip']) === true && empty($_GET['zip']) === false) {
    $sql = $connUls->prepare(
        'SELECT
            zip,
            rec_type,
            primary_city,
            acceptable_cities,
            unacceptable_cities,
            state,
            county,
            timezone,
            area_codes,
            latitude,
            longitude,
            world_region,
            country
        FROM
            zipcodes
        WHERE
            zip = ?;'
    );
    if ($sql->error !== 0) {
        \Sentry\captureMessage($sql->error);
    }

    $zipQuery = $_GET['zip'];
    if ($sql->bind_param('s', $zipQuery) === false) {
            \Sentry\captureMessage($sql->error);
    }
} else if (isset($_GET['county']) === true && isset($_GET['state']) === true
    && empty($_GET['county']) === false && empty($_GET['state']) === false
) {
    $sql = $connUls->prepare(
        'SELECT
            zip,
            rec_type,
            primary_city,
            acceptable_cities,
            unacceptable_cities,
            state,
            county,
            timezone,
            area_codes,
            latitude,
            longitude,
            world_region,
            country
        FROM
            zipcodes
        WHERE
            county LIKE CONCAT("%", ?, "%")
            AND state LIKE CONCAT("%", ?, "%");'
    );
    if ($sql->error !== 0) {
        \Sentry\captureMessage($sql->error);
    }

    $countyQuery = $_GET['county'];
    $stateQuery  = $_GET['state'];
    if ($sql->bind_param('ss', $countyQuery, $stateQuery) === false) {
            \Sentry\captureMessage($sql->error);
    }
} else if (isset($_GET['county']) === true && empty($_GET['county']) === false) {
    $sql = $connUls->prepare(
        'SELECT
            zip,
            rec_type,
            primary_city,
            acceptable_cities,
            unacceptable_cities,
            state,
            county,
            timezone,
            area_codes,
            latitude,
            longitude,
            world_region,
            country
        FROM
            zipcodes
        WHERE
            county LIKE CONCAT("%", ?, "%");'
    );
    if ($sql->error !== 0) {
        \Sentry\captureMessage($sql->error);
    }

    $countyQuery = $_GET['county'];
    if ($sql->bind_param('s', $countyQuery) === false) {
            \Sentry\captureMessage($sql->error);
    }
} else if (isset($_GET['city']) === true && isset($_GET['state']) === true
    && empty($_GET['city']) === false && empty($_GET['state']) === false
) {
    $sql = $connUls->prepare(
        'SELECT
        zip,
        rec_type,
        primary_city,
        acceptable_cities,
        unacceptable_cities,
        state,
        county,
        timezone,
        area_codes,
        latitude,
        longitude,
        world_region,
        country
    FROM
        zipcodes
    WHERE
        (primary_city LIKE CONCAT("%", ?, "%")
        OR acceptable_cities LIKE CONCAT("%", ?, "%"))
        AND state LIKE CONCAT("%", ?, "%");'
    );
    if ($sql->error !== 0) {
        \Sentry\captureMessage($sql->error);
    }

    $cityQuery  = $_GET['city'];
    $stateQuery = $_GET['state'];
    if ($sql->bind_param('sss', $cityQuery, $cityQuery, $stateQuery) === false) {
        \Sentry\captureMessage($sql->error);
    }
}//end if

$sql->execute();
if ($sql->errno > 0) {
    \Sentry\captureMessage($sql->error);
}

$result = $sql->get_result();

if (mysqli_num_rows($result) > 0) {
    $array       = $result->fetch_all(MYSQLI_ASSOC);
    $outputArray = $array;
}


echo json_encode($outputArray);

\Sentry\captureLastError();
