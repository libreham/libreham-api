<?php

/**
 * This is the query LibreHam API query call.
 * This implements the LibreHam APIv0 query.php
 *
 * @package    LibreHam
 * @subpackage query api
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @since      0.0.3
 */

require_once __DIR__ . '/../bootstrap.php'; // Adjust the path as needed

ini_set('memory_limit', '512M');
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
header('Content-Type: application/json; charset=utf-8');

// Create database connection.
$mysqli = new mysqli($_ENV['DB_SERVER'], $_ENV['DB_USER'], $_ENV['DB_PASS']);
if ($mysqli->connect_error) {
    echo json_encode(['error' => 'Database connection failed: ' . $mysqli->connect_error]);
    exit;
}

function validateApiKey(mysqli $mysqli, $apiKey)
{
    $mysqli->select_db('hamdb');
    $stmt = $mysqli->prepare("SELECT api_key FROM api_keys WHERE api_key = ?");
    $stmt->bind_param('s', $apiKey);
    $stmt->execute();
    if ($stmt->get_result()->num_rows === 0) {
        echo json_encode(['status' => 'API Key Invalid']);
        updateStats("badkey");
        exit;
    }
}
function updateStats($status)
{
    $statsFile = "stats.json";
    if (!file_exists($statsFile)) {
        $stats = ["call" => 0, "city" => 0, "name" => 0, "invalid" => 0, "missingkey" => 0, "badkey" => 0];
    } else {
        $stats = json_decode(file_get_contents($statsFile), true);
    }
    if (isset($stats[$status])) {
        $stats[$status]++;
    }
    file_put_contents($statsFile, json_encode($stats));
}

function executeQuery(mysqli $mysqli, $sql, array $params, $dbName)
{
    $mysqli->select_db($dbName);
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param(str_repeat('s', count($params)), ...$params);
    $stmt->execute();
    return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
}
function processRecords($hamdbData, $ulsdbData) {
    $finalResult = [];
    $ulsdbIndexed = array_column($ulsdbData, null, 'callsign');
    // Define a mapping from hide_ flags to the actual fields they should hide
    $hideMapping = [
        'hide_address' => 'street_address',
        'hide_city' => 'city',
        'hide_state' => 'state',
        'hide_postal' => 'postal_code',
        'hide_pobox' => 'po_box',
        'hide_county' => 'county',
        'hide_gridsquare' => 'gridsquare'
        // Add other mappings as needed
    ];

    foreach ($hamdbData as $hamdbRow) {
        $call = $hamdbRow['callsign'];

        $mergedRow = $ulsdbIndexed[$call] ?? [];
        // Merge HAMDB data if is_uls is 'Y' and the data is present in both databases
        if ($hamdbRow['is_uls'] === 'Y' && isset($ulsdbIndexed[$call])) {
            foreach ($hamdbRow as $key => $value) {
                if ($value !== null && $value !== '' && !str_starts_with($key, 'hide_') && $key !== 'is_uls') {
                    $mergedRow[$key] = $value;
                }
            }
        } elseif ($hamdbRow['is_uls'] !== 'Y') {
            // Use HAMDB data directly for non-ULS records or international records
            $mergedRow = $hamdbRow;
        }

        // Apply hide_ logic based on the mapping
        foreach ($hideMapping as $hideKey => $targetField) {
            if (!empty($hamdbRow[$hideKey])) {
                $mergedRow[$targetField] = ''; // Blank the mapped field
            }
        }

        // Remove hide_ flags and is_uls from the result
        foreach ($mergedRow as $key => $value) {
            if (str_starts_with($key, 'hide_') || $key === 'is_uls') {
                unset($mergedRow[$key]);
            }
        }

        $finalResult[$call] = $mergedRow;
    }

    // Add any remaining ULSDB records not already processed
    foreach ($ulsdbIndexed as $call => $ulsRow) {
        if (!isset($finalResult[$call])) {
            $finalResult[$call] = $ulsRow;
        }
    }

    return array_values($finalResult); // Ensure the array is re-indexed numerically
}

$apiKey = $_GET['api_key'] ?? '';
if (empty($apiKey)) {
    echo json_encode(['status' => 'API Key Required']);
    updateStats("missingkey");
    exit;
}

validateApiKey($mysqli, $apiKey);

// Determine the query type and validate against expected options
$queryType = '';
if (!empty($_GET['call'])) {
    $queryType = 'call';
} elseif (!empty($_GET['city'])) {
    $queryType = 'city';
} elseif (!empty($_GET['name'])) {
    $queryType = 'name';
}

// Check if the query type is still empty, meaning it didn't match any expected options
if (empty($queryType)) {
    updateStats("invalid"); // Update stats for invalid query
    header('Location: index.html'); // Redirect to index.html for API documentation
    exit; // Ensure no further script execution after redirection
}

$params = [];
$hamdbSql = $ulsdbSql = '';

switch ($queryType) {
    case 'call':
        $params = [$_GET['call']];
        $hamdbSql = 'SELECT * FROM callsigns WHERE callsign = ? LIMIT 1;';
        $ulsdbSql = 'SELECT AM.callsign, EAT.app_type_name AS app_type, AOC.class_name AS operator_class, HDL.status_name AS license_status, HD.radio_service_code AS radio_service, EN.last_name, EN.first_name, EN.mi, EN.suffix, EN.attention_line, EN.po_box, EN.street_address, EN.city, EN.state, EN.zip_code AS postal_code, HD.grant_date FROM PUBACC_AM AS AM INNER JOIN PUBACC_EN AS EN ON AM.unique_system_identifier = EN.unique_system_identifier INNER JOIN PUBACC_HD AS HD ON AM.unique_system_identifier = HD.unique_system_identifier INNER JOIN EN_ApplicantType AS EAT ON EN.applicant_type_code = EAT.applicant_type_code INNER JOIN AM_OperatorClass AS AOC ON AM.operator_class = AOC.operator_class INNER JOIN HD_LicenseStatus AS HDL ON HD.license_status = HDL.license_status WHERE AM.callsign = ? ORDER BY HD.license_status ASC LIMIT 1;';

        updateStats('call');
        break;
    case 'city':
        $params = ["%" . $_GET['city'] . "%"];
        $hamdbSql = 'SELECT * FROM callsigns WHERE city LIKE ? ORDER BY callsign ASC;';
        $ulsdbSql = 'SELECT AM.callsign, EAT.app_type_name AS app_type, AOC.class_name AS operator_class, HDL.status_name AS license_status, HD.radio_service_code AS radio_service, EN.last_name, EN.first_name, EN.mi, EN.suffix, EN.attention_line, EN.po_box, EN.street_address, EN.city, EN.state, EN.zip_code AS postal_code, HD.grant_date FROM PUBACC_AM AS AM INNER JOIN PUBACC_EN AS EN ON AM.unique_system_identifier = EN.unique_system_identifier INNER JOIN PUBACC_HD AS HD ON AM.unique_system_identifier = HD.unique_system_identifier INNER JOIN EN_ApplicantType AS EAT ON EN.applicant_type_code = EAT.applicant_type_code INNER JOIN AM_OperatorClass AS AOC ON AM.operator_class = AOC.operator_class INNER JOIN HD_LicenseStatus AS HDL ON HD.license_status = HDL.license_status WHERE EN.city LIKE ? ORDER BY AM.callsign ASC;';
        updateStats('city');
        break;
    case 'name':
        $params = ["%" . $_GET['name'] . "%", "%" . $_GET['name'] . "%"];
        $hamdbSql = 'SELECT * FROM callsigns WHERE last_name LIKE ? OR first_name LIKE ? ORDER BY callsign ASC;';
        $ulsdbSql = 'SELECT AM.callsign, EAT.app_type_name AS app_type, AOC.class_name AS operator_class, HDL.status_name AS license_status, HD.radio_service_code AS radio_service, EN.last_name, EN.first_name, EN.mi, EN.suffix, EN.attention_line, EN.po_box, EN.street_address, EN.city, EN.state, EN.zip_code AS postal_code, HD.grant_date FROM PUBACC_AM AS AM INNER JOIN PUBACC_EN AS EN ON AM.unique_system_identifier = EN.unique_system_identifier INNER JOIN PUBACC_HD AS HD ON AM.unique_system_identifier = HD.unique_system_identifier INNER JOIN EN_ApplicantType AS EAT ON EN.applicant_type_code = EAT.applicant_type_code INNER JOIN AM_OperatorClass AS AOC ON AM.operator_class = AOC.operator_class INNER JOIN HD_LicenseStatus AS HDL ON HD.license_status = HDL.license_status WHERE EN.last_name LIKE ? OR EN.first_name LIKE ? ORDER BY AM.callsign ASC;';
        updateStats('name');
        break;
}


$hamdbData = executeQuery($mysqli, $hamdbSql, $params, $_ENV['DB_HAMDB_NAME']);
$ulsdbData = executeQuery($mysqli, $ulsdbSql, $params, $_ENV['DB_ULSDATA_NAME']);
$finalResult = processRecords($hamdbData, $ulsdbData);

echo json_encode($finalResult);

$mysqli->close();
