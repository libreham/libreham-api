<?php
/**
 * This is a script not meant to be run by browser to generate API keys
 *
 * @package    LibreHam
 * @subpackage new api key
 * @author     Matthew Chambers <mchambers@mchambersradio.com>
 * @copyright  2024 LibreHam Project
 * @since      0.0.3
 */

 require_once __DIR__ . 'bootstrap.php';

// Connect to the database
$conn = new mysqli($_ENV['DB_SERVER'], $_ENV['DB_USER'], $_ENV['DB_PASS'], $_ENV('DB_HAMDB_NAME'));

// Check for database connection error
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Function to generate a unique API Key
function generateApiKey() {
    $timestamp = microtime(true);
    $randomStr = bin2hex(random_bytes(10)); // 20 characters
    $apiKey = hash('sha256', $timestamp . $randomStr);
    return $apiKey;
}

// Generate a unique API Key
$apiKey = generateApiKey();

// Prepare an SQL statement to insert the new API Key into the database
$stmt = $conn->prepare("INSERT INTO api_keys (api_key) VALUES (?)");

// Bind the API Key to the prepared statement
$stmt->bind_param("s", $apiKey);

// Execute the statement
if ($stmt->execute()) {
    echo "New API Key generated successfully: " . $apiKey;
} else {
    echo "Error: " . $stmt->error;
}

// Close statement and connection
$stmt->close();
$conn->close();

